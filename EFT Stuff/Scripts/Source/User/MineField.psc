Scriptname MineField extends activemagiceffect

Group Data
spell Property SpellToCast const auto mandatory
{MineFieldExplosion}

float Property CastTimerInterval = 5.0 const auto
{how often should we cast the spell on the actor}

EndGroup


Event OnEffectStart(Actor akTarget, Actor akCaster)
	startTimer(CastTimerInterval)
EndEvent

Function CastSpellAndStartTimer()
	if IsBoundGameObjectAvailable() ;is effect still running on a legit object?
		actor actorRef = GetTargetActor()
		SpellToCast.cast(actorRef, actorRef)
	
		startTimer(CastTimerInterval)
	endif
EndFunction

Event OnTimer(int aiTimerID)
	CastSpellAndStartTimer()
EndEvent
