Scriptname MinePlacer extends activemagiceffect

;-- Properties --------------------------------------
Group Data
		Explosion Property MineExplosion Auto Const
EndGroup

;-- Variables ---------------------------------------


;-- Functions ---------------------------------------

Event OnEffectStart(Actor akTarget, Actor akCaster)
		actor Owner = self.GetTargetActor()
		Owner.placeatme(MineExplosion)
EndEvent
